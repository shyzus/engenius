//package nl.hva.fdmci.tsse.engine.model;
//
//import javafx.application.Platform;
//import javafx.event.EventHandler;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.layout.Pane;
//import javafx.scene.paint.Color;
//import javafx.scene.paint.Paint;
//import javafx.scene.shape.Circle;
//import javafx.scene.shape.Rectangle;
//import nl.hva.fdmci.tsse.engine.service.JBoxUtils;
//import org.jbox2d.collision.shapes.CircleShape;
//import org.jbox2d.collision.shapes.PolygonShape;
//import org.jbox2d.collision.shapes.Shape;
//import org.jbox2d.common.Vec2;
//import org.jbox2d.dynamics.Body;
//import org.jbox2d.dynamics.BodyType;
//import org.jbox2d.dynamics.World;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.testfx.framework.junit5.ApplicationExtension;
//
//@ExtendWith(ApplicationExtension.class)
//public class PhysicsEntityTest {
//
//    private final EntityFactory<Rectangle, PolygonShape> RECTANGLE_FACTORY = new EntityFactory<>();
//    private final EntityFactory<Circle, CircleShape> CIRCLE_FACTORY = new EntityFactory<>();
//    private PhysicsEntity entity;
//
//    @BeforeEach
//    public void setUp() throws Exception {
//        entity = RECTANGLE_FACTORY.createEntity(new Rectangle(25, 25), new PolygonShape());
//    }
//
//    @AfterEach
//    public void tearDown() throws Exception {
//        entity = null;
//    }
//
//    @Test
//    public void setPositionNoOffset() {
//        // Arrange
//        float x = 25;
//        float y = 15;
//
//        // Act
//        entity.setPosition(x, y);
//
//        // Assert
//        Assertions.assertEquals(x - (entity.getWidth() / 2), entity.getLayoutX(), 0.0);
//        Assertions.assertEquals(y - (entity.getHeight() / 2), entity.getLayoutY(), 0.0);
//    }
//
//    @Test
//    public void setPositionWithOffset() {
//        // Arrange
//        float x = 25;
//        float y = 15;
//        float xOffset = 5;
//        float yOffset = 10;
//
//        // Act
//        entity.setPosition(x, xOffset, y, yOffset);
//
//        // Assert
//        Assertions.assertEquals((x - (entity.getWidth() / 2)) - xOffset, entity.getLayoutX(), 0.0);
//        Assertions.assertEquals((y - (entity.getHeight() / 2)) - yOffset, entity.getLayoutY(), 0.0);
//    }
//
//    @Test
//    public void setHeight() {
//        // Arrange
//        int height = 100;
//        Rectangle rectangle = (Rectangle) entity.getJavaFXShape();
//
//        // Act
//        entity.setHeight(height);
//
//        // Assert
//        Assertions.assertEquals(height, rectangle.getHeight(), 0.0);
//    }
//
//    @Test
//    public void setWidth() {
//        // Arrange
//        int width = 100;
//        Rectangle rectangle = (Rectangle) entity.getJavaFXShape();
//
//        // Act
//        entity.setWidth(width);
//
//        // Assert
//        Assertions.assertEquals(width, rectangle.getWidth(), 0.0);
//    }
//
//    @Test
//    public void setRadius() {
//        // Arrange
//        int radius = 25;
//        entity = CIRCLE_FACTORY.createEntity(new Circle(2), new CircleShape());
//        Circle circle = (Circle) entity.getJavaFXShape();
//
//        // Act
//        entity.setRadius(radius);
//
//        // Assert
//        Assertions.assertEquals(radius, circle.getRadius(), 0.0);
//    }
//
//    @Test
//    public void setFillUrl() {
//        // Arrange
//        entity = CIRCLE_FACTORY.createEntity(new Circle(2), new CircleShape());
//
//        // Act
//        entity.setFillUrl("/test.png");
//
//
//        // Assert
//        Assertions.assertNotNull(entity.getJavaFXShape().getFill());
//    }
//
//    @Test
//    public void setFillPaint() {
//        // Arrange
//        Paint expectedPaint = Color.RED;
//
//        // Act
//        entity.setFillPaint(expectedPaint);
//
//        // Assert
//        Assertions.assertEquals(expectedPaint, entity.getJavaFXShape().getFill());
//    }
//
//    @Test
//    public void setBodyType() {
//        // Arrange
//        BodyType expectedBodyType = BodyType.DYNAMIC;
//
//        // Act
//        entity.setBodyType(expectedBodyType);
//
//        // Assert
//        Assertions.assertEquals(expectedBodyType, entity.getBodyDef().type);
//    }
//
//    @Test
//    public void createFixture() {
//        // Arrange
//        float expectedDensity = 1f;
//        float expectedRestitution = 1f;
//        float expectedFriction = 1f;
//
//        // Act
//        entity.createFixture(expectedDensity, expectedFriction, expectedRestitution);
//
//        // Assert
//        Assertions.assertEquals(expectedDensity, entity.getFixtureDef().density, 0.0);
//        Assertions.assertEquals(expectedFriction, entity.getFixtureDef().friction, 0.0);
//        Assertions.assertEquals(expectedRestitution, entity.getFixtureDef().restitution, 0.0);
//
//    }
//
//    @Test
//    public void addToWorldNoGroup() {
//        // Arrange
//        World world = new World(new Vec2(0, 0));
//
//        // Act
//        entity.createFixture(1f, 1f, 1f).addToWorld(world);
//
//        // Assert
//        Assertions.assertEquals(world.getBodyList(), entity.getBody());
//
//    }
//
//    @Test
//    public void addToWorldAndGroup() {
//        // Arrange
//        World world = new World(new Vec2(0, 0));
//        Pane group = new Pane();
//
//        // Act
//        entity.createFixture(1f, 1f, 1f).addToWorld(world, group);
//
//        // Assert
//        Assertions.assertEquals(world.getBodyList(), entity.getBody());
//        Platform.runLater(() -> {
//            Assertions.assertEquals(1, group.getChildren().size());
//        });
//    }
//
//    @Test
//    public void setShapeAsBox() {
//        // Arrange
//        PolygonShape expectedBox = new PolygonShape();
//        expectedBox.setAsBox(JBoxUtils.pixelToMeter(25f / 2), JBoxUtils.pixelToMeter(25f / 2));
//
//        // Act
//        entity.setShapeAsBox(25f, 25f);
//
//        // Assert
//        Assertions.assertArrayEquals(expectedBox.m_vertices, ((PolygonShape) entity.getJbox2DShape()).m_vertices);
//    }
//
//    @Test
//    public void setShapeRadius() {
//        // Arrange
//        CircleShape expectedCircle = new CircleShape();
//        expectedCircle.setRadius(JBoxUtils.pixelToMeter(25f));
//
//        entity = CIRCLE_FACTORY.createEntity(new Circle(25), new CircleShape());
//
//        // Act
//        entity.setShapeRadius(25);
//
//        // Assert
//        Assertions.assertEquals(expectedCircle.m_radius, ((CircleShape) entity.getJbox2DShape()).m_radius, 0.0);
//        Assertions.assertEquals(expectedCircle.m_type, ((CircleShape) entity.getJbox2DShape()).m_type);
//        Assertions.assertEquals(expectedCircle.m_p, ((CircleShape) entity.getJbox2DShape()).m_p);
//
//    }
//
//    @Test
//    public void getJbox2DShape() {
//        // Arrange
//        PolygonShape polygonShape = new PolygonShape();
//        entity = RECTANGLE_FACTORY.createEntity(new Rectangle(2, 2), polygonShape);
//
//        // Act
//        Shape actualShape = entity.getJbox2DShape();
//
//        // Assert
//        Assertions.assertEquals(polygonShape, actualShape);
//    }
//
//    @Test
//    public void getJavaFXShape() {
//        // Arrange
//        Rectangle rectangle = new Rectangle(2, 2);
//        entity = RECTANGLE_FACTORY.createEntity(rectangle, new PolygonShape());
//
//        // Act
//        javafx.scene.shape.Shape actualShape = entity.getJavaFXShape();
//
//        // Assert
//        Assertions.assertEquals(rectangle, actualShape);
//    }
//
//    @Test
//    public void getBody() {
//        // Arrange
//        World world = new World(new Vec2(0, 0));
//        entity.createFixture(1f, 1f, 1f).addToWorld(world);
//
//        // Act
//        Body actualBody = entity.getBody();
//
//        // Assert
//        Assertions.assertEquals(entity.getUserData(), actualBody);
//    }
//
//    @Test
//    public void setDensity() {
//        // Arrange
//        float density = 2f;
//
//        // Act
//        entity.setDensity(density);
//
//        // Assert
//        Assertions.assertEquals(density, entity.getFixtureDef().density, 0.0);
//    }
//
//    @Test
//    public void setFriction() {
//        // Arrange
//        float friction = 2f;
//
//        // Act
//        entity.setFriction(friction);
//
//        // Assert
//        Assertions.assertEquals(friction, entity.getFixtureDef().friction, 0.0);
//    }
//
//    @Test
//    public void setRestitution() {
//        // Arrange
//        float restitution = 2f;
//
//        // Act
//        entity.setRestitution(restitution);
//
//        // Assert
//        Assertions.assertEquals(restitution, entity.getFixtureDef().restitution, 0.0);
//    }
//
//    @Test
//    public void setKeyPressed() {
//        // Arrange
//        EventHandler<KeyEvent> keyEventEventHandler = (keyEvent) -> System.out.println("THIS IS A KEY EVENT");
//
//        // Act
//        entity.setKeyPressed(keyEventEventHandler);
//
//        // Assert
//        Assertions.assertEquals(keyEventEventHandler, entity.getKeyPressed());
//    }
//
//    @Test
//    public void getKeyPressed() {
//        // Arrange
//        EventHandler<KeyEvent> keyEventEventHandler = (keyEvent) -> System.out.println("THIS IS A KEY EVENT");
//        entity.setKeyPressed(keyEventEventHandler);
//
//        // Act
//        EventHandler<KeyEvent> actualKeyEvent = entity.getKeyPressed();
//
//        // Assert
//        Assertions.assertEquals(keyEventEventHandler, actualKeyEvent);
//    }
//
//    @Test
//    public void getKeyReleased() {
//        // Arrange
//        EventHandler<KeyEvent> keyEventEventHandler = (keyEvent) -> System.out.println("THIS IS A KEY EVENT");
//        entity.setKeyReleased(keyEventEventHandler);
//
//        // Act
//        EventHandler<KeyEvent> actualKeyEvent = entity.getKeyReleased();
//
//        // Assert
//        Assertions.assertEquals(keyEventEventHandler, actualKeyEvent);
//    }
//
//    @Test
//    public void setKeyReleased() {
//        // Arrange
//        EventHandler<KeyEvent> keyEventEventHandler = (keyEvent) -> System.out.println("THIS IS A KEY EVENT");
//
//        // Act
//        entity.setKeyReleased(keyEventEventHandler);
//
//        // Assert
//        Assertions.assertEquals(keyEventEventHandler, entity.getKeyReleased());
//    }
//}