//package nl.hva.fdmci.tsse.engine.model;
//
//import javafx.scene.shape.Rectangle;
//import org.jbox2d.collision.shapes.PolygonShape;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.testfx.framework.junit5.ApplicationExtension;
//
//@ExtendWith(ApplicationExtension.class)
//public class EntityFactoryTest {
//
//    @Test
//    public void createEntityCorrect() {
//        // Arrange
//        EntityFactory<Rectangle, PolygonShape> entityFactory = new EntityFactory<>();
//
//        // Act
//        int ENTITY_SIZE = 25;
//        PhysicsEntity entity = entityFactory.createEntity(new Rectangle(ENTITY_SIZE, ENTITY_SIZE), new PolygonShape());
//
//        // Assert
//        Assertions.assertNotNull(entity);;
//    }
//}