package nl.hva.fdmci.tsse.engine.service.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashMap;
import java.util.Map;

/**
 * The LocalStorageService is responsible for handling all input and output on the filesystem accessible to the user.
 *
 * @author A.R Waly <Achmed.Waly@hva.nl>
 */
public class LocalStorageService {
    private static Logger LOG = LogManager.getLogger(LocalStorageService.class);

    // Define storage directory
    public static final String STORAGE_DIRECTORY = System.getProperty("user.home").concat("/.engenius");
    private Map<String, Object> loadedAssets;

    /**
     * Default constructor for the LocalStorageService
     */
    private LocalStorageService() {
        loadedAssets =  new HashMap<>();

        if (!Files.exists(Path.of(STORAGE_DIRECTORY))) {

            // Fetch all attributes for read/write access
            FileAttribute fileAttribute = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxrwxrw-"));

            try {
                Files.createDirectory(Path.of(STORAGE_DIRECTORY), fileAttribute);
            } catch (IOException e) {
                LOG.error("couldn't create Directory: " + STORAGE_DIRECTORY);
                LOG.debug(e);
            }

        }

    }

    /**
     * Fetch a saved object
     *
     * @param url url of the object
     * @return saved object or null
     */
    public Object fetch(String url) {

        if (loadedAssets.containsKey(url)) {
            return loadedAssets.get(url);
        } else {
            File file = new File(url);

            try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
                return objectInputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                LOG.error("couldn't load assets: " + url);
                LOG.debug(e);
            }
        }
        return null;
    }

    /**
     * Save an object
     *
     * @param url url of the object
     * @param object object to save
     */
    public void save(String url, Object object) {
        File file = new File(url);
        loadedAssets.putIfAbsent(url, object);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            LOG.error("couldn't save object to directory: " + url);
            LOG.debug(e);
        }
    }

}
