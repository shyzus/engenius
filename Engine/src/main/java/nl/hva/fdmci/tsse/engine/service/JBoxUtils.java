package nl.hva.fdmci.tsse.engine.service;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import nl.hva.fdmci.tsse.engine.model.PhysicsEntity;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class JBoxUtils {

    // Assuming 50 pixels per meter conversion
    public static float pixelToMeter(float pixels) {
        return 0.02f * pixels;
    }

    public static float meterToPixel(float meters) {
        return 50.0f * meters;
    }


    public static void drawDebug(boolean toggle, Pane root, PhysicsEntity entity) {
        int baseX = (int) meterToPixel(entity.getBody().getPosition().x);
        int baseY = (int) meterToPixel(entity.getBody().getPosition().y) - 100;
        int textSpacing = 15;

        for (Object label : entity.getDebugLabels()) {
            if (label instanceof Label) {
                if (!toggle) {
                    root.getChildren().remove(label);
                    continue;
                }

                if (!root.getChildren().contains(label)) root.getChildren().add( ((Label) label));

                ((Label) label).setTextFill(Color.WHITE);
                ((Label) label).setLayoutX(baseX);
                ((Label) label).setLayoutY(baseY += textSpacing);
            }

        }

        entity.debug();
    }

    public static void resetEntity(PhysicsEntity entity, float x, float y) {
        Body body = entity.getBody();
        body.setTransform(new Vec2(x, y), 0);
        body.m_torque = 0;
        body.m_sweep.a = 0;
        body.setLinearVelocity(new Vec2(0, 0));
    }

}