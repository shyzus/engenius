package nl.hva.fdmci.tsse.engine.service.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesService {

    private final String DEFAULT_FILE_NAME = "config.properties";
    private Class<?> toLookFrom = getClass();
    private final Logger LOG = LogManager.getLogger(PropertiesService.class);

    private Properties loadedProperties;

    public PropertiesService() {
        try {
            loadProperties(DEFAULT_FILE_NAME);
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    public PropertiesService(Class<?> toLookFrom) {
        this();
        this.toLookFrom = toLookFrom;
    }

    /**
     * Load the config.properties file from /resources as a Properties object.
     *
     * @throws IOException Could not load from classpath
     */
    public void loadProperties(String fileName) throws IOException {
        if (loadedProperties == null) {
            loadedProperties = new Properties();
            if (fileName == null || fileName.isBlank()) {
                fileName = DEFAULT_FILE_NAME;
            }
            InputStream inputStream = toLookFrom.getResourceAsStream(fileName);
            if (inputStream != null) {
                loadedProperties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + fileName + "' not found in the classpath");
            }
        }

    }

    public String getPropertyByKey(String key) {
        if (loadedProperties != null) {
            return loadedProperties.getProperty(key);
        } else {
            throw new NullPointerException("No properties were loaded! loadedProperties = null");
        }
    }
}
