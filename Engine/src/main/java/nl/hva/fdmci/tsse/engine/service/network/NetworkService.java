package nl.hva.fdmci.tsse.engine.service.network;

import com.github.thorbenkuck.netcom2.exceptions.ClientConnectionFailedException;
import com.github.thorbenkuck.netcom2.exceptions.StartFailedException;
import com.github.thorbenkuck.netcom2.interfaces.TriConsumer;
import com.github.thorbenkuck.netcom2.logging.Logging;
import com.github.thorbenkuck.netcom2.network.client.ClientStart;
import com.github.thorbenkuck.netcom2.network.client.Sender;
import com.github.thorbenkuck.netcom2.network.server.ServerStart;
import com.github.thorbenkuck.netcom2.network.shared.CommunicationRegistration;
import com.github.thorbenkuck.netcom2.network.shared.Session;
import com.github.thorbenkuck.netcom2.network.shared.connections.ConnectionContext;

import java.util.concurrent.Executors;

/**
 * Network Service for high-level usage of NetCom2 Client & Server functionality
 *
 * @author Achmed Waly
 */
public class NetworkService {

    private ServerStart serverInstance;
    private ClientStart clientInstance;

    /**
     * Launch the server with the given port and starts accepting all clients.
     *
     * @param port port number
     */
    public void launchServer(int port) throws StartFailedException {

        serverInstance = ServerStart.at(port);
        serverInstance.setLogging(Logging.info());
        serverInstance.launch();
        Executors.newCachedThreadPool().execute(() -> {
            try {
                serverInstance.acceptAllNextClients();
            } catch (ClientConnectionFailedException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Launch the client using the hostname and port of the server
     *
     * @param hostname hostname of the server
     * @param port     port of the server
     */
    public void launchClient(String hostname, int port) throws StartFailedException {

        clientInstance = ClientStart.at(hostname, port);
        clientInstance.setLogging(Logging.info());
        clientInstance.launch();
    }

    public void killServerNow() {
        if (serverInstance != null && serverInstance.running()) {
            serverInstance.disconnect();
            serverInstance = null;
        }
    }

    public void killClientNow() {
        if (clientInstance != null && clientInstance.running()) {
            clientInstance.softStop();
            clientInstance = null;
        }
    }

    public CommunicationRegistration getServerCommunicationRegistration() {
        if (serverInstance != null) {
            return serverInstance.getCommunicationRegistration();
        } else {
            return null;
        }
    }

    public CommunicationRegistration getClientCommunicationRegistration() {
        if (clientInstance != null) {
            return clientInstance.getCommunicationRegistration();
        } else {
            return null;
        }
    }

    public boolean isServerRunning() {
        if (serverInstance != null) {
            return serverInstance.running();
        } else {
            return false;
        }
    }

    public boolean isClientRunning() {
        if (clientInstance != null) {
            return clientInstance.running();
        } else {
            return false;
        }
    }

    public ServerStart getServerInstance() {
        return serverInstance;
    }

    public ClientStart getClientInstance() {
        return clientInstance;
    }

    public void registerServerPipeline
            (Class<?> classToRegister, TriConsumer<ConnectionContext, Session, Object> action) {
        serverInstance.getCommunicationRegistration().register(classToRegister).addLast(action::accept);
    }

    public void unregisterServerPipeline(Class<?> classToUnregister) {
        serverInstance.getCommunicationRegistration().unRegister(classToUnregister);
    }

    public void unregisterClientPipeline(Class<?> classToUnregister) {
        clientInstance.getCommunicationRegistration().unRegister(classToUnregister);
    }

    public void registerClientPipeline
            (Class<?> classToRegister, TriConsumer<ConnectionContext, Session, Object> action) {
        clientInstance.getCommunicationRegistration().register(classToRegister).addLast(action::accept);
    }

    public Sender getClientSender() {
        return Sender.open(clientInstance);
    }

}
