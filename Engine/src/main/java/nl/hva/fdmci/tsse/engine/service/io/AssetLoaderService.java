package nl.hva.fdmci.tsse.engine.service.io;

import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;

/**
 * AssetLoader class
 *
 * Responsible for quickly retrieving and caching assets in case they are requested again.
 * Default behaviour is to store retrieved objects in cache for quicker later retrieval.
 *
 * @author A.R. Waly
 */
public class AssetLoaderService {

    private final Logger LOG = LogManager.getLogger(AssetLoaderService.class);
    private Class<?> toLookFrom;
    public Map<String, Object> loadedAssets;

    /**
     * Default constructor;
     */
    public AssetLoaderService() {
        loadedAssets = new HashMap<>();
        toLookFrom = AssetLoaderService.class;
    }

    public AssetLoaderService(Class<?> classToLookFrom) {
        this();
        this.toLookFrom = classToLookFrom;
    }


    private InputStream getResourceAsStream(String url) {
        return toLookFrom.getResourceAsStream(url);
    }

    private URL getResourceAsURL(String url) {
        return toLookFrom.getResource(url);
    }

    /**
     * Load an packaged asset by url.
     *
     * @param url string
     * @return retrieved object.
     */
    public Image loadAssetFromClass(String url, boolean caching) {

        if (caching && loadedAssets.containsKey(url)) {
            return (Image) loadedAssets.get(url);
        } else {
            Image image = new Image(getResourceAsStream(url));
            loadedAssets.put(url, image);
            return image;
        }

    }

    public List<URL> loadAssetsFromDirectory(String path) throws IOException {
        Enumeration<URL> en = toLookFrom.getClassLoader().getResources(
                path);

        List<URL> urls = new ArrayList<>();
        List<String> filenames = new ArrayList<>();

        try (
                InputStream in = getResourceFolderAsStream(path);
                BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            String resource;

            while ((resource = br.readLine()) != null) {
                filenames.add(resource);
            }
        }

        URL url = en.nextElement();
        for(String name : filenames) {
            urls.add(new URL(url+name));
        }

        return urls;
    }

    private InputStream getResourceFolderAsStream(String resource) {
        final InputStream in
                = getContextClassLoader().getResourceAsStream(resource);

        return in == null ? getClass().getResourceAsStream(resource) : in;
    }

    private InputStream getResourceFolderAsStream(String resource, Class<?> classToUse) {
        final InputStream in
                = classToUse.getResourceAsStream(resource);

        return in == null ? classToUse.getResourceAsStream(resource) : in;
    }

    private ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

}