package nl.hva.fdmci.tsse.engine.model;

import javafx.scene.shape.Shape;

/**
 * The EntityFactory is responsible for creating PhysicsEntity that use a Shape from the OpenJFX library and
 * another shape from the JBox2D library.
 *
 * @see javafx.scene.shape.Shape
 * @see org.jbox2d.collision.shapes.Shape
 *
 * @param <X> Class that extends javafx.scene.shape.Shape
 * @param <Y> Class that extends org.jbox2d.collision.shapes.Shape
 */
public interface EntityFactory<X extends Shape, Y extends org.jbox2d.collision.shapes.Shape> {

    /**
     * Create an entity using the current EntityFactory
     *
     * @param javaFXShape Object that is an instance/extension of javafx.scene.shape.Shape
     * @param jbox2DShape Object that is an instance/extension of org.jbox2d.collision.shapes.Shape
     * @return Basic PhysicsEntity object
     */
    PhysicsEntity<? extends Shape,? extends org.jbox2d.collision.shapes.Shape> createEntity(X javaFXShape, Y jbox2DShape);

}
