//package nl.hva.fdmci.tsse.engine.service;
//
//import javafx.scene.media.AudioClip;
//import javafx.scene.media.Media;
//import javafx.scene.media.MediaPlayer;
//import nl.hva.fdmci.tsse.engine.service.io.AssetLoaderService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * AudioBank class
// *
// * @author JdeJong <jeroen--j@hotmail.com>
// */
//public class AudioBank {
//
//    private static Logger LOG = LogManager.getLogger(AudioBank.class);
//
//    // Map for all short audio clips with a few playback controls, these clips are kept in memory.
//    private Map<String, AudioClip> clipBank;
//    // Map for all longer audio files with more playback controls, these will be streamed from disc when played.
//    private Map<String, Media> mediaBank;
//    // Map for MediaPlayers, for easier usage of Media objects from mediaBank.
//    private Map<String, MediaPlayer> mPlayerBank;
//
//    private static final String AUDIO_DIR = "audio/";
//    private static final String MEDIA_DIR = "long/";
//
//    /**
//     * Gets called only when boolean doLoadAudioClips is set to true, which it is not by default.
//     * Can also be called manually using Container::loadAudioClips.
//     * Populates clipBank with all audio found in resources/audio.
//     */
//    public void loadAudioClips() {
//        try {
//            clipBank = loadAudioClips(AUDIO_DIR);
//        } catch (IOException | URISyntaxException e) {
//            LOG.error("couldn't load audio clips");
//            LOG.debug(e);
//        }
//    }
//
//    /**
//     * Gets called only when boolean doLoadAudioMedia is set to true, which it is not by default.
//     * Can also be called manually using Container::loadAudioMedia.
//     * Populates clipBank with all audio found in resources/audio/long.
//     */
//    public void loadAudioMedia() {
//        try {
//            mediaBank = loadAudioMedia(AUDIO_DIR + MEDIA_DIR);
//            mPlayerBank = new HashMap<>();
//        } catch (IOException | URISyntaxException e) {
//            LOG.error("couldn't load audio media");
//            LOG.debug(e);
//        }
//    }
//
//    /**
//     * Puts all entries in mediaBank into mPlayerBank as MediaPlayers. Doesn't get called by default, but can be called
//     * on for optimization.
//     */
//    public void loadMediaPlayers() {
//        for(Map.Entry<String, Media> entry : mediaBank.entrySet()) {
//            mPlayerBank.put(entry.getKey(), new MediaPlayer(entry.getValue()));
//        }
//    }
//
//    /**
//     * Lists the contents of the resources/audio folder and loads them into a HashMap for AudioClips which it returns.
//     * @param path Should end with "/", but not start with one.
//     * @return Just the name of each member item, not the full paths.
//     *
//     * @throws URISyntaxException URISyntaxException
//     * @throws IOException IOException
//     */
//    private HashMap<String, AudioClip> loadAudioClips(String path) throws URISyntaxException, IOException {
//        URL dirURL = getClass().getClassLoader().getResource(path);
//        if (dirURL != null && dirURL.getProtocol().equals("file")) {
//            HashMap<String, AudioClip> bank = new HashMap<>();
//            String[] fileNames = new File(dirURL.toURI()).list();
//            if (fileNames != null && fileNames.length > 0) {
//                for (String fileName : fileNames) {
//                    if (fileName.endsWith(".m4a") || fileName.endsWith(".wav") || fileName.endsWith(".aiff") || fileName.endsWith(".au") || fileName.endsWith(".aac") || fileName.endsWith(".mp3")) {
//                        bank.put(fileName, (AudioClip) AssetLoaderService.getInstance().loadAsset("/" + path + fileName, AudioClip.class));
//                    }
//                }
//            }
//            return bank;
//        } else {
//            throw new IOException();
//        }
//    }
//
//    /**
//     * Lists the contents of the resources/audio/long folder and loads them into a HashMap for Media which it returns.
//     * @param path Should end with "/", but not start with one.
//     * @return Just the name of each member item, not the full paths.
//     *
//     * @throws URISyntaxException URISyntaxException
//     * @throws IOException IOException
//     */
//    private HashMap<String, Media> loadAudioMedia(String path) throws URISyntaxException, IOException {
//        URL dirURL = getClass().getClassLoader().getResource(path);
//        if (dirURL != null && dirURL.getProtocol().equals("file")) {
//            HashMap<String, Media> bank = new HashMap<>();
//            String[] fileNames = new File(dirURL.toURI()).list();
//            if (fileNames != null && fileNames.length > 0) {
//                for (String fileName : fileNames) {
//                    if (fileName.endsWith(".m4a") || fileName.endsWith(".wav") || fileName.endsWith(".aiff") || fileName.endsWith(".au") || fileName.endsWith(".aac") || fileName.endsWith(".mp3")) {
//                        bank.put(fileName, (Media) AssetLoaderService.getInstance().loadAsset("/" + path + fileName, Media.class));
//                    }
//                }
//            }
//            return bank;
//        } else {
//            throw new IOException();
//        }
//    }
//
//    public Map<String, AudioClip> getClipBank() {
//        return clipBank;
//    }
//
//    public Map<String, Media> getMediaBank() {
//        return mediaBank;
//    }
//
//    public Map<String, MediaPlayer> getMediaPlayers() {
//        return mPlayerBank;
//    }
//
//    public AudioClip clip(String key) {
//        return clipBank.get(key);
//    }
//
//    public Media media(String key) {
//        return mediaBank.get(key);
//    }
//
//    /**
//     * Gets a MediaPlayer from the mPlayerBank for usability purposes.
//     * If the given key does not correspond to an entry in mPlayerBank, this method will create a MediaPlayer
//     * with the media of the same key from mediaBank, put it in mPlayerBank and return the MediaPlayer.
//     * @param key the name of the audio file.
//     * @return a MediaPlayer containing the Media corresponding to the key.
//     */
//    public MediaPlayer mediaP(String key) {
//        if (mPlayerBank.containsKey(key)) {
//            return mPlayerBank.get(key);
//        } else {
//            MediaPlayer mp = new MediaPlayer(media(key));
//            mPlayerBank.put(key, mp);
//            return mp;
//        }
//    }
//}
