package nl.hva.fdmci.tsse.engine.model;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import nl.hva.fdmci.tsse.engine.service.JBoxUtils;
import nl.hva.fdmci.tsse.engine.service.TileMapService;
import nl.hva.fdmci.tsse.engine.service.io.PropertiesService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * This class represents a level that is used to create a game.
 * A basic level renders a tilemap created by a user using the Tiled map editor.
 * The level class provides the user with multiple options to add physics to a level
 * author Koen Hengsdijk <koen.hengsdijk@hva.nl>
 */
public class Level extends Pane {

    private TileMap tileMap;

    private World world;

    private final double STANDARD_SCALE = 1;

    private final int TILE_ID_OFFSET = 1;

    private double scale;

    private int tileSize = 32;

    private List<PhysicsEntity<? extends Shape, ? extends org.jbox2d.collision.shapes.Shape>> entityList;

    private EntityFactory<Rectangle, PolygonShape> entityFactory;

    private final Logger LOG = LogManager.getLogger(getClass());

    /**
     *
     */
    public Level(TileMap tileMap, EntityFactory<Rectangle, PolygonShape> entityFactory) {
        super();
        this.tileMap = tileMap;
        world = new World(new Vec2(0, 0));
        scale = STANDARD_SCALE;
        entityList = new ArrayList<>();
        this.entityFactory = entityFactory;
    }

    public Level(int tileSize, int levelWidth, int levelHeight, EntityFactory<Rectangle, PolygonShape> entityFactory) {
        super();
        this.tileSize = tileSize;
        world = new World(new Vec2(0, 0));
        scale = STANDARD_SCALE;
        entityList = new ArrayList<>();
        this.entityFactory = entityFactory;
    }

    /**
     * method to initialise all the map after configuration has been done.
     */
    public Level build() {
        if (tileMap != null) {
            TileMapService.setLoadedTileMap(tileMap);
            tileMap.init();
            tileMap.getLayerList().forEach(layer -> {
                layer.getTiles().forEach(tile -> {
                    if (tile.getBodyType() != null && (tile.getBodyType().equals(BodyType.STATIC.toString())
                            || tile.getBodyType().equals(BodyType.DYNAMIC.toString())
                            || tile.getBodyType().equals(BodyType.KINEMATIC.toString())
                    )) {
                        var newEntity = entityFactory.createEntity(tile.getRectangle(), new PolygonShape())
                                .setPosition((tile.getCoordinateX() * tileSize) + 16, (tile.getCoordinateY() * tileSize) + 16)
                                .setShapeAsBox(tileSize, tileSize)
                                .createFixture(1f,1f,0.1f)
                                .addToWorld(world);
                        entityList.add(newEntity);
                    }
                });
            });
            tileMap.addTileMapToPane(this);
        }

        for (PhysicsEntity< ? extends Shape, ? extends org.jbox2d.collision.shapes.Shape> ent : entityList) {
            getChildren().add(ent);
        }

        return this;
    }

    /**
     * add physics to a specific tile
     *
     */
    public Level addPhysicsUsingSpecifiedCoordinates( PhysicsEntity physicsThing) {
        entityList.add(physicsThing);
        return this;
    }

    /**
     *
     * @param x the column of the tile
     * @param y the row of the tile
     * @param density the density of the physics object that is added
     * @param friction the friction of the physics object that is added
     * @param restitution the restitution of the physics object that is added
     * @return the level itself so that builder pattern can be used
     */
    public Level addPhysicsUsingColumnAndRow(int x, int y, float density, float friction, float restitution) {

        int xPos = x * tileSize;
        int yPos = y * tileSize;


        PhysicsEntity ent = entityFactory.createEntity(new Rectangle(tileSize, tileSize), new PolygonShape())
                .setBodyType(BodyType.STATIC)
                .setPosition(xPos, yPos)
                .createFixture(density, friction, restitution)
                .setFillPaint(Color.YELLOW)
                .setShapeAsBox(tileSize, tileSize)
                .addToWorld(world);

        entityList.add(ent);

        return this;
    }

    /**
     * add physics to a region
     *
     * @param x  the column of the first tile
     * @param y  the row of the first tile
     * @param x2 the column of the second tile
     * @param y2 the row of the second tile
     * @param density the density of the physics object that is added
     * @param friction the friction of the physics object that is added
     * @param restitution the restitution of the physics object that is added
     * @return the level itself so that builder pattern can be used
     */
    public Level addPhysicsToRegion(int x, int y, int x2, int y2, float density, float friction, float restitution) {

        int initialXcoordinate = x * tileSize;
        int initialYcoordinate = y * tileSize;

        if (x > x2) {
            initialXcoordinate = x2 * tileSize;
        }
        if (y > y2) {
            initialYcoordinate = y2 * tileSize;
        }

        int xDifference = Math.abs(x - x2);
        int yDifference = Math.abs(y - y2);

        int physicsWidth = xDifference * tileSize;
        int physicsHeight = yDifference * tileSize;

        PhysicsEntity<? extends Shape, ? extends org.jbox2d.collision.shapes.Shape> ent = entityFactory.createEntity(new Rectangle(physicsWidth, physicsHeight), new PolygonShape())
                .setBodyType(BodyType.STATIC)
                .setPosition(JBoxUtils.pixelToMeter(initialXcoordinate), JBoxUtils.pixelToMeter(initialYcoordinate))
                .createFixture(density, friction, restitution)
                .setFillPaint(Color.TRANSPARENT)
                .setShapeAsBox(physicsWidth, physicsHeight)
                .addToWorld(world);

        entityList.add(ent);

        return this;
    }

    public Level setScale(double scale) {
        this.scale = scale;
        return this;

    }

    //This method creates a walls.
    public void addWall(float posX, float posY, float width, float height){
        PolygonShape ps = new PolygonShape();
        ps.setAsBox(JBoxUtils.pixelToMeter(width),JBoxUtils.pixelToMeter(height));

        FixtureDef fd = new FixtureDef();
        fd.shape = ps;
        fd.density = 1f;
        fd.friction = 0f;

        BodyDef bd = new BodyDef();
        bd.position.set(JBoxUtils.pixelToMeter(posX), JBoxUtils.pixelToMeter(posY));

        world.createBody(bd).createFixture(fd);
    }

    public PhysicsEntity addWall(float posX, float posY, float width, float height, Pane root){

        return entityFactory.createEntity(new Rectangle(width, height), new PolygonShape())
                .setPosition(posX, posY)
                .setShapeAsBox(width * 2, height * 2)
                .setBodyType(BodyType.STATIC)
                .createFixture(1f, 0f, 0f)
                .addToWorld(world, root);
    }

    public World getWorld() {
        return world;
    }

    public TileMap getTileMap() {
        return tileMap;
    }

    public List<PhysicsEntity<? extends Shape, ? extends org.jbox2d.collision.shapes.Shape>> getEntityList() {
        return entityList;
    }

}

