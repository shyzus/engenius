package nl.hva.fdmci.tsse.engine.service;


import nl.hva.fdmci.tsse.engine.model.TileMap;

public class TileMapService {

    static TileMap loadedTileMap;

    private TileMapService() {

    }

    public static void setLoadedTileMap(TileMap tileMap) {
        loadedTileMap = tileMap;
    }

    public static TileMap getLoadedTileMap() {
        return loadedTileMap;
    }
}
