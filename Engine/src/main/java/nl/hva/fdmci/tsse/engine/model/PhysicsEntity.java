package nl.hva.fdmci.tsse.engine.model;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import nl.hva.fdmci.tsse.engine.service.JBoxUtils;
import nl.hva.fdmci.tsse.engine.service.io.AssetLoaderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.dynamics.*;

import java.io.Serializable;

public class PhysicsEntity<X extends javafx.scene.shape.Shape, Y extends Shape> extends Parent implements Cloneable, Serializable {

    private transient Shape jbox2DShape;
    private transient BodyDef bodyDef = new BodyDef();
    private transient FixtureDef fixtureDef = new FixtureDef();
    private transient Body body;
    private transient javafx.scene.shape.Shape javaFXShape;
    private String name;

    private transient EventHandler<KeyEvent> keyPressed;
    private transient EventHandler<KeyEvent> keyReleased;

    private final transient Label xVelocityLabel = new Label();
    private final transient Label yVelocityLabel = new Label();
    private final transient Label positionLabel = new Label();
    private final transient Label typeLabel = new Label();
    private final transient Label angleLabel = new Label();
    private final transient Label torqueLabel = new Label();

    private final transient ObservableList<Label> debugLabels = FXCollections.observableArrayList();

    private final transient Logger LOG = LogManager.getLogger(getClass());

    public PhysicsEntity(X javaFXShape, Y jbox2DShape) {
        super();
        this.jbox2DShape = jbox2DShape;
        this.javaFXShape = javaFXShape;
        getChildren().add(javaFXShape);
        debugLabels.add(xVelocityLabel);
        debugLabels.add(yVelocityLabel);
        debugLabels.add(positionLabel);
        debugLabels.add(angleLabel);
        debugLabels.add(torqueLabel);
    }

    private PhysicsEntity<X,Y> setX(float x) {
        if (body == null) {
            setLayoutX(x - (getWidth()/2));
            bodyDef.position.set(JBoxUtils.pixelToMeter(x), bodyDef.position.y);
        } else {
            setLayoutX(JBoxUtils.meterToPixel(x));
        }

        return this;
    }

    private PhysicsEntity<X,Y> setY(float y) {
        if (body == null) {
            setLayoutY(y - (getHeight()/2));
            bodyDef.position.set(bodyDef.position.x, JBoxUtils.pixelToMeter(y));
        } else {
            setLayoutY(JBoxUtils.meterToPixel(y));
        }
        return this;
    }

    public PhysicsEntity<X,Y> setPosition(float x, float y) {
        setX(x);
        setY(y);
        return this;
    }

    public PhysicsEntity<X,Y> setPosition(float x, float xOffset, float y, float yOffset) {
        setX(x - xOffset);
        setY(y - yOffset);
        return this;
    }

    public PhysicsEntity<X,Y> setHeight(double height) {
        if (javaFXShape instanceof Rectangle) {
            ((Rectangle) javaFXShape).setHeight(height);
        } else {
            LOG.debug("Setting height of a non-rectangle object is not allowed!");
        }
        return this;
    }

    public PhysicsEntity<X,Y> setWidth(double width) {
        if (javaFXShape instanceof Rectangle) {
            ((Rectangle) javaFXShape).setWidth(width);
        } else {
            LOG.debug("Setting width of a non-rectangle object is not allowed!");
        }
        return this;
    }

    public PhysicsEntity<X,Y> setRadius(double radius) {
        if (javaFXShape instanceof Circle) {
            ((Circle) javaFXShape).setRadius(radius);
        } else {
            LOG.debug("Setting radius of a non-circle object is not allowed!");
        }
        return this;
    }

    public double getHeight() throws UnsupportedOperationException {
        if (javaFXShape instanceof Rectangle) {
            return ((Rectangle) javaFXShape).getHeight();
        } else {
            throw new UnsupportedOperationException("Setting height of a non-rectangle object is not allowed!");
        }
    }

    public double getWidth() throws UnsupportedOperationException {
        if (javaFXShape instanceof Rectangle) {
            return ((Rectangle) javaFXShape).getWidth();
        } else {
            throw new UnsupportedOperationException("Setting width of a non-rectangle object is not allowed!");
        }
    }

    public double getRadius() throws UnsupportedOperationException {
        if (javaFXShape instanceof Circle) {
            return ((Circle) javaFXShape).getRadius();
        } else {
            throw new UnsupportedOperationException("Setting radius of a non-circle object is not allowed!");
        }
    }

    public PhysicsEntity<X,Y> setFillUrl(String url, Image image) {
        javaFXShape.setFill(new ImagePattern(image));
        return this;
    }

    public PhysicsEntity<X,Y> setFillPaint(Paint paint) {
        javaFXShape.setFill(paint);
        return this;
    }

    public PhysicsEntity<X,Y> setBodyType(BodyType bodyType) {
        bodyDef.type = bodyType;
        return this;
    }

    public PhysicsEntity<X,Y> createFixture(float density, float friction, float restitution) {
        fixtureDef.shape = jbox2DShape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        return this;
    }

    public PhysicsEntity<X,Y> addToWorld(World world, Pane pane) {
        body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        setUserData(body);
        body.setUserData(this);
        Platform.runLater(() -> pane.getChildren().add(this));
        return this;
    }

    public PhysicsEntity<X,Y> addToWorld(World world) {
        body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        setUserData(body);
        body.setUserData(this);
        return this;
    }

    public PhysicsEntity<X,Y> setShapeAsBox(float width, float height) {
        if (jbox2DShape instanceof PolygonShape) {
            ((PolygonShape) jbox2DShape).setAsBox(JBoxUtils.pixelToMeter(width / 2), JBoxUtils.pixelToMeter(height / 2));
        } else {
            LOG.debug("Setting shape as box on non-polygon shapes is not allowed.");
        }

        return this;
    }

    public PhysicsEntity<X,Y> setShapeRadius(float radius) {
        if (jbox2DShape instanceof PolygonShape) {
            LOG.debug("Setting shape radius on non-circle shapes is not allowed");
        } else {
            jbox2DShape.setRadius(JBoxUtils.pixelToMeter(radius));
        }
        return this;
    }

    public Shape getJbox2DShape() {
        return jbox2DShape;
    }

    public javafx.scene.shape.Shape getJavaFXShape() {
        return javaFXShape;
    }

    public Body getBody() {
        return body;
    }

    public PhysicsEntity<X,Y> setDensity(float density) {
        if (body == null) {
            createFixture(density, fixtureDef.friction, fixtureDef.restitution);
        } else {
            body.getFixtureList().setDensity(density);
        }
        return this;
    }

    public PhysicsEntity<X,Y> setFriction(float friction) {
        if (body == null) {
            createFixture(fixtureDef.density, friction, fixtureDef.restitution);
        } else {
            body.getFixtureList().setFriction(friction);
        }
        return this;
    }

    public PhysicsEntity<X,Y> setRestitution(float restitution) {
        if (body == null) {
            createFixture(fixtureDef.density, fixtureDef.friction, restitution);
        } else {
            body.getFixtureList().setRestitution(restitution);
        }
        return this;
    }

    public BodyDef getBodyDef() {
        return bodyDef;
    }

    public FixtureDef getFixtureDef() {
        return fixtureDef;
    }

    public void setKeyPressed(EventHandler<KeyEvent> keyPressed) {
        this.keyPressed = keyPressed;
    }

    public EventHandler<KeyEvent> getKeyPressed() {
        return keyPressed;
    }

    public EventHandler<KeyEvent> getKeyReleased() {
        return keyReleased;
    }

    public void setKeyReleased(EventHandler<KeyEvent> keyReleased) {
        this.keyReleased = keyReleased;
    }

    public ObservableList<Label> getDebugLabels() {
        return debugLabels;
    }

    public void debug() {
        xVelocityLabel.setText("X velocity: " + body.getLinearVelocity().x);
        yVelocityLabel.setText("Y velocity: " + body.getLinearVelocity().y);
        positionLabel.setText("X: " + body.getPosition().x + " Y: " + body.getPosition().y);
        angleLabel.setText("Angle: " + body.getAngle());
        typeLabel.setText("Body type: " + body.getType());
        torqueLabel.setText("Torque: " + body.m_torque);
    }

    public PhysicsEntity<X,Y> clone() throws CloneNotSupportedException {
        return (PhysicsEntity<X,Y>) super.clone();
    }

    public String getName() {
        return name;
    }

    public PhysicsEntity<X,Y> setName(String name) {
        this.name = name;
        return this;
    }
}
