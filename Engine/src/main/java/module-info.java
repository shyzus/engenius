module engenius {

    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.media;
    requires org.apache.logging.log4j;
    requires javafx.swing;
    requires NetCom2;
    requires jbox2d.library;
    requires java.logging;
    requires org.apache.commons.io;
    requires java.sql;
    requires org.apache.commons.lang3;
    requires com.google.gson;

    opens nl.hva.fdmci.tsse.engine.model;

    exports nl.hva.fdmci.tsse.engine.service.io;
    exports nl.hva.fdmci.tsse.engine.model;
    exports nl.hva.fdmci.tsse.engine.service;
    exports nl.hva.fdmci.tsse.engine.service.network;
}