package nl.hva.fdmci.ttse.example;

import com.github.thorbenkuck.netcom2.exceptions.StartFailedException;
import com.github.thorbenkuck.netcom2.network.server.Distributor;
import nl.hva.fdmci.tsse.engine.service.network.NetworkService;
import nl.hva.fdmci.tsse.example.shared.model.Request;

public class Driver {

    public static void main(String[] args) {
        NetworkService networkService = new NetworkService();
        try {
            networkService.launchServer(38924);
        } catch (StartFailedException e) {
            e.printStackTrace();
        }

        Distributor distributor = Distributor.open(networkService.getServerInstance());

        networkService.registerServerPipeline(Request.class, (connectionContext, session, o) -> {
            if (o instanceof Request) {
                if (((Request) o).getIdentifier().equals("move")) {
                    ((Request) o).getPayload().forEach( (name, value) -> System.out.println(name + ": " + value));
                    distributor.toAll(o);
                }
            }
        });
    }
}
