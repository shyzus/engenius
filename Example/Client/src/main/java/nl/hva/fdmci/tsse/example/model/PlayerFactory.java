package nl.hva.fdmci.tsse.example.model;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import nl.hva.fdmci.tsse.engine.model.EntityFactory;
import nl.hva.fdmci.tsse.engine.model.PhysicsEntity;
import org.jbox2d.collision.shapes.PolygonShape;

public class PlayerFactory implements EntityFactory<Rectangle, PolygonShape> {
    @Override
    public Player createEntity(Rectangle javaFXShape, PolygonShape jbox2DShape) {
        return new Player(new Rectangle(), new PolygonShape());
    }
}
