package nl.hva.fdmci.tsse.example.controller;

import com.github.thorbenkuck.netcom2.exceptions.StartFailedException;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import nl.hva.fdmci.tsse.engine.model.Level;
import nl.hva.fdmci.tsse.engine.model.PhysicsEntity;
import nl.hva.fdmci.tsse.engine.model.TileMap;
import nl.hva.fdmci.tsse.engine.service.JBoxUtils;
import nl.hva.fdmci.tsse.engine.service.TileMapService;
import nl.hva.fdmci.tsse.engine.service.io.AssetLoaderService;
import nl.hva.fdmci.tsse.engine.service.io.JSONService;
import nl.hva.fdmci.tsse.engine.service.io.LocalStorageService;
import nl.hva.fdmci.tsse.engine.service.network.NetworkService;
import nl.hva.fdmci.tsse.example.model.Player;
import nl.hva.fdmci.tsse.example.model.PlayerFactory;
import nl.hva.fdmci.tsse.example.shared.model.Request;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyType;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

public class ExampleController implements Initializable {

    @FXML
    private Pane ContentPane;

    @FXML
    private ListView<RadioButton> DebugLV;

    private Level level;
    private final List<Player> ENTITIES = new ArrayList<>();
    private final Timeline timeline = new Timeline();
    private final Button btn = new Button();
    private final List<KeyCode> KEYS_PRESSED = new ArrayList<>();
    private Player controllableEntity;
    private int width;
    private int height;
    private final PlayerFactory RECTANGLE_FACTORY = new PlayerFactory();
    private final JSONService JSON_SERVICE = new JSONService();
    private NetworkService networkService;
    private final Logger LOG = Logger.getLogger(getClass().getName());
    private RadioButton leftBtn;
    private RadioButton rightBtn;
    private RadioButton upBtn;
    private RadioButton downBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            level = new Level((TileMap) JSON_SERVICE.readFromFileToObject(LocalStorageService.STORAGE_DIRECTORY.concat("/test-tilemap.json"), TileMap.class), RECTANGLE_FACTORY);
            TileMapService.setLoadedTileMap(level.getTileMap());
            level.build();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        width = level.getTileMap().getTopLayer().getColumnCount() * 32;
        height = level.getTileMap().getTopLayer().getRowCount() * 32;
        level.addWall(0, height, width, 0, level); // ground
        level.getWorld().setGravity(new Vec2(0, 0));
        level.addWall(0, height, 0, height, level); //Left wall
        level.addWall(width, height,0, height, level); //Right wall
        level.addWall(width , 0, width, 0, level); // roof

        level.getWorld().setAutoClearForces(true);

        ContentPane.getChildren().add(level);

        AssetLoaderService assetLoaderService = new AssetLoaderService(getClass());

        controllableEntity = (Player) RECTANGLE_FACTORY.createEntity(new Rectangle(32,32), new PolygonShape())
                .setBodyType(BodyType.DYNAMIC)
                .setFillUrl("big_demon_idle_anim_f0.png", assetLoaderService.loadAssetFromClass("/big_demon_idle_anim_f0.png", true))
                .setHeight(32)
                .setWidth(32)
                .setShapeAsBox(32f,32f)
                .setPosition(64f,64f)
                .createFixture(1f,0f,0f)
                .addToWorld(level.getWorld(), ContentPane);

        controllableEntity.toFront();
        controllableEntity.setSpeed(10f);
        controllableEntity.getBody().setFixedRotation(true);

        addMoving(controllableEntity);
        addKeyReleasedEvent(controllableEntity);

        ENTITIES.add(controllableEntity);

        defineStep();
        defineTriggers();
        initNetworking();
        initDebug();
    }

    private void initDebug() {
        leftBtn = new RadioButton("Left");
        rightBtn = new RadioButton("Right");
        upBtn = new RadioButton("Up");
        downBtn = new RadioButton("Down");

        DebugLV.getItems().addAll(leftBtn,rightBtn,upBtn,downBtn);
        DebugLV.getItems().forEach(radioButton -> radioButton.setFocusTraversable(false));
    }

    private void defineStep() {
        timeline.setCycleCount(Timeline.INDEFINITE);

        Duration duration = Duration.seconds(1.0 / 60f); // Set duration for frame.

        //Create an ActionEvent, on trigger it executes a world time step and moves the balls to new position
        EventHandler<ActionEvent> ae = t -> {

            //Create time step. Set Iteration count 8 for velocity and 10 for positions
            level.getWorld().step(1.0f / 60f, 8, 10);
            for (Player entity : ENTITIES) {
                entity.toFront();
                //Move balls to the new position computed by JBox2D
                Body body = entity.getBody();

                if (KEYS_PRESSED.isEmpty()) {
                    body.setLinearVelocity(new Vec2(0, 0));
                }

                JBoxUtils.drawDebug(true, ContentPane, entity);

                Platform.runLater(() -> {
                    float xOffset = 0;
                    float yOffset = 0;

                    if (entity.getJavaFXShape() instanceof Rectangle) {
                        xOffset = JBoxUtils.pixelToMeter((float) ((Rectangle) entity.getJavaFXShape()).getWidth() / 2f);
                        yOffset = JBoxUtils.pixelToMeter((float) ((Rectangle) entity.getJavaFXShape()).getHeight() / 2f);
                    }

                    entity.setPosition(body.getPosition().x, xOffset, body.getPosition().y, yOffset);
                    entity.setRotate(-body.getAngle());
                });
            }

        };

        Platform.runLater( () -> {
            ContentPane.getScene().setOnKeyPressed(controllableEntity.getKeyPressed());
            ContentPane.getScene().setOnKeyReleased(controllableEntity.getKeyReleased());
        });

        /*
         * Set ActionEvent and duration to the KeyFrame.
         * The ActionEvent is triggered when KeyFrame execution is over.
         */
        KeyFrame frame = new KeyFrame(duration, ae, null, null);

        timeline.getKeyFrames().add(frame);
    }

    private void defineTriggers() {
        btn.setLayoutX(width / 2);
        btn.setLayoutY(height - 30);
        btn.setText("Start");
        btn.setOnAction(event -> {
            timeline.playFromStart();
            btn.setVisible(false);
        });

        //Add button to the root group
        ContentPane.getChildren().add(btn);
    }

    private void addMoving(Player entity) {
        EventHandler<KeyEvent> handler = x -> {
            KeyCode pressedKey = x.getCode();

            if (pressedKey == (KeyCode.LEFT) && !KEYS_PRESSED.contains(KeyCode.LEFT)) {
                KEYS_PRESSED.add(KeyCode.LEFT);
                move(KeyCode.LEFT.toString(), -entity.getSpeed());
                leftBtn.fire();
            }

            if (pressedKey == (KeyCode.RIGHT) && !KEYS_PRESSED.contains(KeyCode.RIGHT)) {
                KEYS_PRESSED.add(KeyCode.RIGHT);
                move(KeyCode.RIGHT.toString(), entity.getSpeed());
                rightBtn.fire();
            }

            if (pressedKey == (KeyCode.UP) && !KEYS_PRESSED.contains(KeyCode.UP)) {
                KEYS_PRESSED.add(KeyCode.UP);
                move(KeyCode.UP.toString(), -entity.getSpeed());
                upBtn.fire();
            }

            if (pressedKey == (KeyCode.DOWN) && !KEYS_PRESSED.contains(KeyCode.DOWN)) {
                KEYS_PRESSED.add(KeyCode.DOWN);
                move(KeyCode.DOWN.toString(), entity.getSpeed());
                downBtn.fire();
            }
        };

        entity.setKeyPressed(handler);
    }

    private void addKeyReleasedEvent(Player entity) {
        EventHandler<KeyEvent> handler = x -> {
            KeyCode pressedKey = x.getCode();

            if (pressedKey == (KeyCode.LEFT)) {
                LOG.log(INFO, KEYS_PRESSED.toString());
                KEYS_PRESSED.remove(KeyCode.LEFT);
                move(KeyCode.LEFT.toString(), 0);
                leftBtn.fire();
            }

            if (pressedKey == (KeyCode.RIGHT)) {
                LOG.log(INFO, KEYS_PRESSED.toString());
                KEYS_PRESSED.remove(KeyCode.RIGHT);
                move(KeyCode.RIGHT.toString(), 0);
                rightBtn.fire();
            }

            if (pressedKey == (KeyCode.UP)) {
                LOG.log(INFO, KEYS_PRESSED.toString());
                KEYS_PRESSED.remove(KeyCode.UP);
                move(KeyCode.UP.toString(), 0);
                upBtn.fire();
            }

            if (pressedKey == (KeyCode.DOWN)) {
                LOG.log(INFO, KEYS_PRESSED.toString());
                KEYS_PRESSED.remove(KeyCode.DOWN);
                move(KeyCode.DOWN.toString(), 0);
                downBtn.fire();
            }
        };

        entity.setKeyReleased(handler);
    }

    private void initNetworking() {
        networkService = new NetworkService();
        try {
            networkService.launchClient("localhost", 38924);
            networkService.registerClientPipeline(Request.class, (connectionContext, session, o) -> {
                if (o instanceof Request) {
                    Request request = (Request) o;
                    if (request.getIdentifier().equals("move")) {
                        if (request.getPayloadById("direction").equals("UP") || request.getPayloadById("direction").equals("DOWN")) {
                            controllableEntity.getBody().setLinearVelocity(new Vec2(controllableEntity.getBody().getLinearVelocity().x, (Float) request.getPayloadById("distance")));
                            //controllableEntity.getBody().applyForceToCenter(new Vec2(controllableEntity.getBody().getLinearVelocity().x, (Float) request.getPayloadById("distance")));
                            LOG.log(INFO, "Y movement");
                        } else if (request.getPayloadById("direction").equals("LEFT") || request.getPayloadById("direction").equals("RIGHT")) {
                            controllableEntity.getBody().setLinearVelocity(new Vec2((Float) request.getPayloadById("distance"), controllableEntity.getBody().getLinearVelocity().y));
                            //controllableEntity.getBody().applyForceToCenter(new Vec2((Float) request.getPayloadById("distance"), controllableEntity.getBody().getLinearVelocity().y));
                            LOG.log(INFO, "X movement");
                        }
                    }
                }
            });
        } catch (StartFailedException e) {
            e.printStackTrace();
        }
    }

    private void move(String direction, float distance) {
        if (networkService.isClientRunning()) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("direction", direction);
            hashMap.put("distance", distance);
            networkService.getClientSender().objectToServer(new Request("move", hashMap));
        } else {
            LOG.log(SEVERE, "Client is not connected!");
        }
    }
}