package nl.hva.fdmci.tsse.example.model;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import nl.hva.fdmci.tsse.engine.model.PhysicsEntity;
import org.jbox2d.collision.shapes.PolygonShape;

import java.io.Serializable;

public class Player extends PhysicsEntity<Rectangle, PolygonShape> implements Serializable {

    private float speed;

    public Player(Rectangle javaFXShape, PolygonShape jbox2DShape) {
        super(javaFXShape, jbox2DShape);
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

}
