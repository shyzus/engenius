module Example.Client {

    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires engenius;
    requires jbox2d.library;
    requires NetCom2;
    requires Example;
    requires java.logging;

    exports nl.hva.fdmci.tsse.example to javafx.graphics;
    exports nl.hva.fdmci.tsse.example.controller to javafx.fxml;

    opens nl.hva.fdmci.tsse.example.controller to javafx.fxml;
}