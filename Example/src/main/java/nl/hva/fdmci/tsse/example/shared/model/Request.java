package nl.hva.fdmci.tsse.example.shared.model;

import java.io.Serializable;
import java.util.HashMap;

public class Request implements Serializable {

    private String identifier;
    private HashMap<String, Object> payload;

    public Request(String identifier) {
        this.identifier = identifier;
    }

    public Request(String identifier, HashMap<String, Object> payload) {
        this.identifier = identifier;
        this.payload = payload;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public HashMap<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(HashMap<String, Object> payload) {
        this.payload = payload;
    }

    public Object getPayloadById(String identifier) {
        return payload.get(identifier);
    }
}
